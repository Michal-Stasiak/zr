base_number = float(input("Enter a base: "))
exponent_number = float(input("Enter an exponent: "))
sum = base_number ** exponent_number
print(f"{base_number} to the power of {exponent_number} = {sum}")