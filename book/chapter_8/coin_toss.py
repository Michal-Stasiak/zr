import random


def coin_flip():
    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"


for trail in range(10_000):
    heads_tally = 0
    tails_tally = 0
    counter = 0
    while heads_tally <= 1 and tails_tally <= 1:
        counter += 1
        if coin_flip() == "heads":
            heads_tally += 1
        else:
            tails_tally += 1
    print(counter)
