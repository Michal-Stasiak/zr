import random


def regional_election(probability_of_win):
    if random.random() < probability_of_win:
        return "a"
    else:
        return "b"


def election(region_1, region_2, region_3):
    if (regional_election(region_1) and regional_election(region_2) == "a") or (regional_election(region_2) and regional_election(region_3) == "a"):
        # print("Candidate A won!")
        return "a"
    else:
        return "b"


candidate_a = 0
counter = 0
for trial in range(5):
    counter += 1
    if election(.87, .65, .17) == "a":
        candidate_a += 1

ratio = candidate_a / counter * 100
print(f"Candidate A won {ratio}% times")
