def invest(amount, rate, years):
    for n in range(1, years + 1):
        amount = amount + amount * rate
        print(f"year {n}: ${amount:.2f}")


user_amount = float(input("Enter an initial amunt: "))
user_rate = float(input("Enter an annual percentage rate: "))
user_years = int(input("Enter a number years: "))
invest(user_amount, user_rate, user_years)