def convert_cel_to_far(cel):
    far = cel * 9 / 5 + 32
    return far

def convert_far_to_cel(far):
    cel = (far - 32) * 5 / 9
    return cel

user_far = int(input("Enter a temperature in Fahrenheit: "))
converted_far_to_cel = convert_far_to_cel(user_far)
print(f"{user_far} degrees F = {converted_far_to_cel:.2f} degrees C")

user_cel = int(input("Enter a temperature in Celsius: "))
converted_cel_to_far = convert_cel_to_far(user_cel)
print(f"{user_cel} degrees C = {converted_cel_to_far:.2f} degrees F")