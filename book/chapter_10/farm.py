class Animal():
    def __init__(self, name):
        self.name = name

    def speak(self, sound):
        return f"{self.name} makes: {sound}"

    def type_of_animal(self, type_of_animal):
        return f"{self.name} is a type of {type_of_animal}"


class Chicken(Animal):
    def speak(self, sound="ko ko"):
        return super().speak(sound)

    def eggs(self, number_off_eggs):
        return f"{self.name} laid {number_off_eggs} eggs"

    def type_of_animal(self, type_of_animal="poundry"):
        return super().type_of_animal(type_of_animal)


class Cow(Animal):
    def speak(self, sound="muuu"):
        return super().speak(sound)

    def type_of_animal(self, type_of_animal="dairy"):
        return super().type_of_animal(type_of_animal)

    def milk(self, liters_of_milk):
        return f"{self.name} gave {liters_of_milk} liters of milk"


class Horse(Animal):
    rode = 0

    def speak(self, sound="yhyhyh"):
        return super().speak(sound)

    def type_of_animal(self, type_of_animal="riding"):
        return super().type_of_animal(type_of_animal)

    def walk(self, km):
        self.rode += km
        return f"{self.name} walked {km} kilometers"


chick = Chicken("Chick")
cow = Cow("Mucka")
horse = Horse("Kun")

print(chick.eggs(12))
print(cow.type_of_animal())
print(cow.milk(2))
print(horse.walk(22))