class Car:
    def __init__(self, color, mileage):
        self.color = color
        self.mileage = mileage

    def drive(self, number):
        self.mileage += number


blue = Car("blue", 20000)
red = Car("red", 30000)
print(f"The {blue.color} car has {blue.mileage:,} miles")
print(f"The {red.color} car has {red.mileage:,} miles")
porshe = Car("silver", 0)
print(f"{porshe.mileage}")
porshe.drive(100)
print(f"{porshe.mileage}")

