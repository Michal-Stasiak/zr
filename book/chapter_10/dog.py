class Dog:
    species = "Canis familiaris"

    def __init__(self, name, age, ):
        self.name = name
        self.age = age
        # self.coat_color = coat_color

    def __str__(self):
        return f"{self.name} is {self.age} years old."

    def speak(self, sound):
        return f"{self.name} says {sound}"


class JackRusselTerrier(Dog):
    def speak(self, sound = "Arf"):
        # return f"{self.name} says {sound}"
        return super().speak(sound)


class Dashmund(Dog):
    pass


class Bulldog(Dog):
    pass


class GoldenRetriever(Dog):
    def speak(self, sound = "Bark"):
        return super().speak(sound)


miles = JackRusselTerrier("Miles", 4)
buddy = Dashmund("Buddy", 9)
jack = Bulldog("Jack", 3)
jim = Bulldog("Jim", 5)
goldi = GoldenRetriever("Goldi", 30)
print(miles.speak())
print(miles.speak("Grr"))
print(goldi.speak())