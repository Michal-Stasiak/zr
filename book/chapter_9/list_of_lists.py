list_of_universities = [
    ["California Institute of Technology", 2175, 37704],
    ["Harvard", 19627, 39849],
    ["Massachusetts Institute of Technology", 10566, 40732],
    ["Princeton", 7802, 37000],
    ["Rice", 5879, 40569],
    ["Stanfrod", 19535, 40569],
    ["Yale", 11701, 40500]
]


def enrollment_stats(universities):
    enrollments = [university[1] for university in universities]
    tuition_fees = [university[2] for university in universities]
    return enrollments, tuition_fees


def mean(list_of_values):
    mean_of_the_list = sum(list_of_values)/len(list_of_values)
    return mean_of_the_list


def median(list_of_values):
    list_of_values.sort()
    middle_element = int(len(list_of_values) / 2)
    median_of_the_list = list_of_values[middle_element]
    return median_of_the_list


enrollments, tuition_fees = enrollment_stats(list_of_universities)
print(f"Total students: {sum(enrollments)}")
print(f"Total tuition: ${sum(tuition_fees):,}")
print(f"Student mean: {mean(enrollments):,.2f}")
print(f"Student median: {median(enrollments):,}")
print(f"Tuition mean: ${mean(tuition_fees):,.2f}")
print(f"Tuition median: ${median(tuition_fees):,.2f}")
