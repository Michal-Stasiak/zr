# cardinal_numbers = ("first", "second", "third")
# print(cardinal_numbers[1])
# position1, position2, position3 = cardinal_numbers[0], cardinal_numbers[1], cardinal_numbers[2]
# print(position1)
# print(position2)
# print(position3)

# my_name = tuple("michal")
# "x" in my_name
# almost_my_name = my_name[1:]
# print(almost_my_name)

# groceries = "eggs, milk, cheese"
# grocery_list = groceries.split(", ")
# print(grocery_list)
# grocery_list.insert(1, "chocolate")
# print(grocery_list)
# grocery_list.pop(0)
# print(grocery_list)
# grocery_list.pop()
# print(grocery_list)
# grocery_list.extend(["cocoa", "bananas"])
# print(grocery_list)
# grocery_list.extend(('cos', "nieco"))
# print(grocery_list)
# grocery_list.pop()
# print(grocery_list)
#
# # List Comprehensions
#
# numbers = (1, 2, 3, 4, 5)
# squares = [num**2 for num in numbers]
# print(squares)
#
# str_numbers = ["1.5", "2.3", "5.25"]
# float_numbers = [float(value) for value in str_numbers]
# print(float_numbers)

food = ["rice", "beans"]
food.append("broccoli")
food.extend(("bread", "pizza"))
print(food[:2])
print(food[-1])
breakfast = "eggs, fruit, orange juice".split(", ")
print(len(breakfast))
lengths = [len(value) for value in breakfast]
print(lengths)