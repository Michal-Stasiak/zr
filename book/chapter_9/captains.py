captains = {}
captains["Enterprise"] = "Picard"
captains["Voyager"] = "Janeway"
captains["Defiant"] = "Sisko"

if "Enterprise" in captains:
    print("ok")
else:
    captains["Enterprise"] = "unknown"

if "Discovery" in captains:
    print("ok")
else:
    captains["Discovery"] = "unknown"

for ship in captains:
    print(f"The {ship} is captained by {captains[ship]}")

del captains["Discovery"]

for ship in captains:
    print(f"The {ship} is captained by {captains[ship]}")