import random

nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]


def generate_poem():
    adj1 = random.choice(adjectives)
    adj2 = random.choice(adjectives)
    adj3 = random.choice(adjectives)
    noun1 = random.choice(nouns)
    noun2 = random.choice(nouns)
    noun3 = random.choice(nouns)
    verb1 = random.choice(verbs)
    verb2 = random.choice(verbs)
    verb3 = random.choice(verbs)
    prep1 = random.choice(prepositions)
    prep2 = random.choice(prepositions)
    adverb1 = random.choice(adverbs)

    article1 = "A" if adj1[0] in ['a', 'e', 'i', 'o', 'u'] else "An"

    poem = f"{article1} {adj1} {noun1}\n"
    poem += f"{article1} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}\n"
    poem += f"{adverb1}, the {noun1} {verb2}\n"
    poem += f"the {noun2} {verb3} {prep2} a {adj3} {noun3}"

    return poem


print(generate_poem())