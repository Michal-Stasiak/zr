import random

capitals_dict = {
    'Albama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
}

state = random.choice(list(capitals_dict))
capital = capitals_dict[state]


while True:
    user_input = input(f"Please enter the capitol of {state}:").lower()
    if user_input == "exit":
        print(f"The capital of {state} is {capital}.\nGoodbye")
        break
    if user_input != capital.lower():
        print("Wrong!")
    if user_input == capital.lower():
        print("Correct")
        break



