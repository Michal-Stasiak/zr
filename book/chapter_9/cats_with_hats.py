list_of_cats = ["off"] * 100
length = len(list_of_cats)

for i in range(1, length + 1):
    for j in range(1, length+1):
        if j % i == 0:
            if list_of_cats[j-1] == "off":
                list_of_cats[j-1] = "on"
            else:
                list_of_cats[j-1] = "off"

for i in range(0, length):
    if list_of_cats[i] == "on":
        print(f"Cat number {i+1} at index {i} has a hat")
