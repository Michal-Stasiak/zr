import hangman_art

print(hangman_art.logo)
chosen_word = input("Choose a word to guess for other player: ").lower()
display = ["_"] * len(chosen_word)
print(display)
lives = len(hangman_art.stages)
while lives > 0:
    guess = input("Guess a letter: ").lower()
    if guess in display:
        print(f"You've already guessed {guess}")
    if guess in chosen_word:
        for n in range(len(chosen_word)):
            if chosen_word[n] == guess:
                display[n] = guess
                print(display)
    else:
        lives -= 1
        print(hangman_art.stages[lives])
    if "_" not in display:
        print("You've won!")
        break
    if lives == 0:
        print("Game over ...")